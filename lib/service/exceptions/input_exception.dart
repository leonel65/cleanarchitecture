class NotNumberException extends Error {
  final message = 'The entered value is not a number';
}

class NotInRangeException extends Error {
  final message = 'Tthe number must be greater than 1 and less than 10';
}